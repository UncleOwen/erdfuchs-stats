"""
File with main function. Mostly for experimentation for now.
"""
import os

from dotenv import load_dotenv

from erdfuchs.api import Api, ApiCore
from erdfuchs.generated.models.DLRequest import DLRequest
from erdfuchs.generated.models.GTITime import GTITime
# from erdfuchs.generated.models.CNRequest import CNRequest
from erdfuchs.generated.models.SDName import SDName
# from erdfuchs.generated.models.DLFilterEntry import DLFilterEntry


def main() -> None:
    """
    main function. does stuff.
    """
    load_dotenv('secrets.env')

    core = ApiCore(
        os.environ['USERNAME'],
        os.environ['PASSWORD'],
    )
    api = Api(core)

    resp = api.departureList(DLRequest(
        station=SDName(id="Master:86961", type="STATION"),  # Schnelsen
        time=GTITime(time="jetzt", date="heute"),
        maxTimeOffset=120,
        maxList=10,
        # filter=[DLFilterEntry(serviceID="AKN")],
    ))

    # resp = api.checkName(CNRequest(
    #    filterType="NO_FILTER",
    #    theName=SDName(name="Schnelsen"),
    #    maxList=10,
    # ))

    print(resp)
    for departure in resp.departures or []:
        print(departure)


if __name__ == '__main__':
    main()
